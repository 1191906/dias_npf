/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class VerPedidosController implements Initializable {

    @FXML
    private Label lbl_nomeCliente;
    @FXML
    private TableView<Pedido> tbVPedidos;
    @FXML
    private TableColumn<Pedido, Integer> clnID;
    @FXML
    private TableColumn<Pedido, String> clnNumPedido;
    @FXML
    private TableColumn<Pedido, String> clnEstado;
    @FXML
    private TableColumn<Pedido, ?> clnEstado1;
    @FXML
    private TableColumn<Pedido, ?> clndetalhes;
    @FXML
    private TableColumn<Pedido, ?> clnHora;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            lbl_nomeCliente.setText(Helper.getLoginUser().getNomeUser());
            tableViewShow();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(VerPedidosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    
    public void tableViewShow() throws ClassNotFoundException, SQLException{
        int id = Helper.getLoginUser().getIdUser();
        ObservableList<Pedido> pp = FXCollections.observableArrayList(Pedido.getListaPedidosByCliente(id));
        
        tbVPedidos.setItems(pp);
        clnID.setCellValueFactory(new PropertyValueFactory("idPedido"));
        clnNumPedido.setCellValueFactory(new PropertyValueFactory("numPedido"));
        clnEstado.setCellValueFactory(new PropertyValueFactory("tipoPedido"));
        clnEstado1.setCellValueFactory(new PropertyValueFactory("dataPedido"));
        clndetalhes.setCellValueFactory(new PropertyValueFactory("detalhesPedidos"));
        clnHora.setCellValueFactory(new PropertyValueFactory("horaPedido"));
        
    }

}
