/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class GerirPratoChefeController implements Initializable {


    @FXML
    private Label lbl_nomeCliente;
    @FXML
    private TableView<Pratos> tbVPedidos;
    @FXML
    private Button btnGravar;
    @FXML
    private TableColumn<Pratos, ?> clnDescricao;
    @FXML
    private TableColumn<Pratos, ?> clnPreco;
    @FXML
    private TextField txtDescricao;
    @FXML
    private TextField txtPreco;
    private Pratos item;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            lbl_nomeCliente.setText(Helper.getLoginUser().getNomeUser());
            tableViewShowNUpdate();

        } catch (ClassNotFoundException | SQLException | InterruptedException ex) {
                Logger.getLogger(GerirPratoChefeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    //botao gravar
    @FXML
    private void btnGravar_click(ActionEvent event) throws SQLException, ClassNotFoundException, InterruptedException {
        if(tbVPedidos.getSelectionModel().isEmpty()){
            Helper.showErrorAlert("Selecione um prato a alterar!");
        }else{
        String descricao = txtDescricao.getText();
        String precoS = txtPreco.getText();
        double preco = Double.parseDouble(precoS);
        System.out.println(descricao+" "+preco);
        int idPrato = item.getIdPrato();
        
        Helper.InsertUpdateDeleteBD("UPDATE DIAS_grupo10.Pratos SET descricaoPrato = '"+descricao+"', precoPrato = '"+preco+"' WHERE idPrato = "+idPrato);
        tableViewShowNUpdate();
        }
    }
//apresentar na tableView
    public void tableViewShowNUpdate() throws ClassNotFoundException, SQLException, InterruptedException{
        int id = Helper.getChefePedidos().getIdRestaurante_chefe();
        ObservableList<Pratos> pp = FXCollections.observableArrayList(Pratos.getListaPratos(id));
        
        tbVPedidos.setItems(pp);
        clnDescricao.setCellValueFactory(new PropertyValueFactory("descricaoPrato"));
        clnPreco.setCellValueFactory(new PropertyValueFactory("precoPrato"));   

    }
//ao clique do mouse
    @FXML
    private void click(MouseEvent event) {
        item = tbVPedidos.getSelectionModel().getSelectedItem();
        String descricao = item.getDescricaoPrato();
        double preco = item.getPrecoPrato();
        txtDescricao.setText(descricao);
        txtPreco.setText(String.valueOf(preco));
        
    }
    

}
