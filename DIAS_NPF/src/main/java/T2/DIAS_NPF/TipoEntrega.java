/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author bruno
 */
public class TipoEntrega {
    
    private int idEntrega;
    private String tipoEntrega;
    private String DescricaoEntrega;
    private double precoEntrega;
    private static final String SQL_SELECT = "SELECT * FROM DIAS_grupo10.TipoEntrega ";
    public int getIdEntrega() {
        return idEntrega;
    }

    public void setIdEntrega(int idEntrega) {
        this.idEntrega = idEntrega;
    }

    public String getTipoEntrega() {
        return tipoEntrega;
    }

    public void setTipoEntrega(String tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }

    public String getDescricaoEntrega() {
        return DescricaoEntrega;
    }

    public void setDescricaoEntrega(String DescricaoEntrega) {
        this.DescricaoEntrega = DescricaoEntrega;
    }

    public double getPrecoEntrega() {
        return precoEntrega;
    }

    public void setPrecoEntrega(double precoEntrega) {
        this.precoEntrega = precoEntrega;
    }
    
    public static ArrayList<TipoEntrega> getListaTipoEntrega() throws ClassNotFoundException, SQLException {
        ArrayList<TipoEntrega> lst1 = new ArrayList();
            
        Connection conn = ConnDB.getConnDB();
        

            PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);    
            ResultSet rs = preparedStatement.executeQuery(SQL_SELECT);

                while (rs.next()) {
                    
                    TipoEntrega item = new TipoEntrega();
                    item.setIdEntrega(rs.getInt("idEntrega"));
                    item.setTipoEntrega(rs.getString("tipoEntrega"));
                    item.setPrecoEntrega(rs.getDouble("precoEntrega"));

                    lst1.add(item);
                }
                preparedStatement.close();
        return lst1;
    } 
        @Override
    public String toString() {
        return this.tipoEntrega;
    }
}
