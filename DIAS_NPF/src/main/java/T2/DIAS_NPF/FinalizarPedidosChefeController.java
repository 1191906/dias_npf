/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class FinalizarPedidosChefeController implements Initializable {


    @FXML
    private Label lbl_nomeCliente;
    @FXML
    private TableView<Pedido> tbVPedidos;
    @FXML
    private TableColumn<Pedido, ?> clnID;
    @FXML
    private TableColumn<Pedido, ?> clnNumPedido;
    @FXML
    private TableColumn<Pedido, ?> clnEstado1;
    @FXML
    private TableColumn<Pedido, ?> clnHora;
    @FXML
    private TableColumn<Pedido, ?> clndetalhes;
    @FXML
    private Button btnFinalizarPedido;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            lbl_nomeCliente.setText(Helper.getLoginUser().getNomeUser());
            tableViewShowNUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FinalizarPedidosChefeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    //Botão para finalizar pedido
    @FXML
    private void btnFinalizarPedido_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException {
        if (tbVPedidos.getSelectionModel().getSelectedItem() == null) {
            Helper.showErrorAlert("Necessário selecionar um pedido!");
        }else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("NPF");
            alert.setHeaderText("Está preste a concluir o pedido do cliente!");
            alert.setContentText("Deseja mesmo continuar?");

            ButtonType buttonTypeYES = new ButtonType("Sim");
            ButtonType buttonTypeNO = new ButtonType("Não");
            alert.getButtonTypes().setAll( buttonTypeYES ,buttonTypeNO); 

            Optional<ButtonType> result = alert.showAndWait();        
            if (result.get() == buttonTypeYES) {                
                FinalizarPedido();
            } else {
                event.consume();
            }
        tableViewShowNUpdate();
        }
    }
    
    //apresentar na tableView os atributos da tabela na base dados
    public void tableViewShowNUpdate() throws ClassNotFoundException, SQLException{
        int id = Helper.getChefePedidos().getIdRestaurante_chefe();
        ObservableList<Pedido> pp = FXCollections.observableArrayList(Pedido.getListaPedidosEmProcessamentoParaConcluir(id));
        
        tbVPedidos.setItems(pp);
        clnID.setCellValueFactory(new PropertyValueFactory("idPedido"));
        clnNumPedido.setCellValueFactory(new PropertyValueFactory("numPedido"));
        clnEstado1.setCellValueFactory(new PropertyValueFactory("dataPedido"));
        clnHora.setCellValueFactory(new PropertyValueFactory("horaPedido"));
        clndetalhes.setCellValueFactory(new PropertyValueFactory("detalhesPedidos"));
        
    }
    
    public void FinalizarPedido() throws SQLException, ClassNotFoundException{
        Pedido selectedItem = tbVPedidos.getSelectionModel().getSelectedItem();
        int idPedido = selectedItem.getIdPedido();
        int tipoEntrega = selectedItem.getTipoPedidoEntrega();
        
        if(tipoEntrega == 2){
        String cmd = "UPDATE `DIAS_grupo10`.`Pedido`"
            + "SET `tipoPedido` = 'Concluido' , `descricaoPedido` = 'Novo Pedido' , `dataPedido` = current_date(),`horaPedido` = current_time() , `estadoPedido` = '5'" 
            + "WHERE `idPedido` =" + idPedido;
        
        Helper.InsertUpdateDeleteBD(cmd);
        Helper.showInfoAlert("Pedido concluido e entregue ao cliente!");
        }else if(tipoEntrega == 1 | tipoEntrega == 3){
            String cmd = "UPDATE `DIAS_grupo10`.`Pedido`"
            + "SET `tipoPedido` = 'Pronto' , `descricaoPedido` = 'Novo Pedido' , `dataPedido` = current_date(),`horaPedido` = current_time() , `estadoPedido` = '4'" 
            + "WHERE `idPedido` =" + idPedido;
            
        Helper.InsertUpdateDeleteBD(cmd);
        Helper.showInfoAlert("Pedido pronto, a espera do Voluntário para conclir a entrega!");
        }
    }    
}
