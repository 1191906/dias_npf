/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import static T2.DIAS_NPF.Utilizadores.SQL_SELECT_USER;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class Restaurantes {
    
    private int idRestaurante;
    private String nome_rest;
    private String morada_rest;
    private int telefone_rest;
    private int nif_rest;
    private String website_rest;
    
    public static final String SQL_SELECT_RESTAURANTE = "SELECT * FROM DIAS_grupo10.Restaurantes WHERE NOT idRestaurante=0";
    
    Restaurantes(){}

    public int getIdRestaurante() {
        return idRestaurante;
    }

    public void setIdRestaurante(int idRestaurante) {
        this.idRestaurante = idRestaurante;
    }

    public String getNome_rest() {
        return nome_rest;
    }

    public void setNome_rest(String nome_rest) {
        this.nome_rest = nome_rest;
    }

    public String getMorada_rest() {
        return morada_rest;
    }

    public void setMorada_rest(String morada_rest) {
        this.morada_rest = morada_rest;
    }

    public int getTelefone_rest() {
        return telefone_rest;
    }

    public void setTelefone_rest(int telefone_rest) {
        this.telefone_rest = telefone_rest;
    }

    public int getNif_rest() {
        return nif_rest;
    }

    public void setNif_rest(int nif_rest) {
        this.nif_rest = nif_rest;
    }

    public String getWebsite_rest() {
        return website_rest;
    }

    public void setWebsite_rest(String website_rest) {
        this.website_rest = website_rest;
    }
    
    public Restaurantes(int idRestaurante, String nome_rest, String morada_rest, int telefone_rest, int nif_rest, String website_rest) {
        this.idRestaurante = idRestaurante;
        this.nome_rest = nome_rest;
        this.morada_rest = morada_rest;
        this.telefone_rest = telefone_rest;
        this.nif_rest = nif_rest;
        this.website_rest = website_rest;
    }
    
    public static List<Restaurantes> getListaRestaurantes() throws SQLException, ClassNotFoundException {
        List<Restaurantes> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
                 
        try (PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT_RESTAURANTE)) {
            
            ResultSet rs = preparedStatement.executeQuery(SQL_SELECT_RESTAURANTE);
                
            while (rs.next()) {
                    
                Restaurantes item = new Restaurantes();
                item.setIdRestaurante(rs.getInt("idRestaurante"));
                item.setNome_rest(rs.getString("nome_rest"));
                item.setMorada_rest(rs.getString("morada_rest"));
                item.setTelefone_rest(rs.getInt("telefone_rest"));
                item.setNif_rest(rs.getInt("nif_rest"));
                item.setWebsite_rest(rs.getString("website_rest"));
                    
                lst.add(item);
            }
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }
    
    @Override
    public String toString() {
        return this.nome_rest;
    }
    
}
