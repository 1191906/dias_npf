/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class Pedido {
    
    private int idPedido;
    private String numPedido;
    private String tipoPedido;
    private String descricaoPedido;
    private int tipoPedidoEntrega;
    private int estadoPedido;
    private Date dataPedido;
    private Time horaPedido;
    private String detalhesPedidos;
    private int idVoluntarioResponsavel;
    private int idRestPedido;

    Pedido(){}

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(String numPedido) {
        this.numPedido = numPedido;
    }

    public String getTipoPedido() {
        return tipoPedido;
    }

    public void setTipoPedido(String tipoPedido) {
        this.tipoPedido = tipoPedido;
    }

    public String getDescricaoPedido() {
        return descricaoPedido;
    }

    public void setDescricaoPedido(String descricaoPedido) {
        this.descricaoPedido = descricaoPedido;
    }

    public int getTipoPedidoEntrega() {
        return tipoPedidoEntrega;
    }

    public void setTipoPedidoEntrega(int tipoPedidoEntrega) {
        this.tipoPedidoEntrega = tipoPedidoEntrega;
    }

    public int getEstadoPedido() {
        return estadoPedido;
    }

    public void setEstadoPedido(int estadoPedido) {
        this.estadoPedido = estadoPedido;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public Time getHoraPedido() {
        return horaPedido;
    }

    public void setHoraPedido(Time horaPedido) {
        this.horaPedido = horaPedido;
    }

    public String getDetalhesPedidos() {
        return detalhesPedidos;
    }

    public void setDetalhesPedidos(String detalhesPedidos) {
        this.detalhesPedidos = detalhesPedidos;
    }

    public int getIdVoluntarioResponsavel() {
        return idVoluntarioResponsavel;
    }

    public void setIdVoluntarioResponsavel(int idVoluntarioResponsavel) {
        this.idVoluntarioResponsavel = idVoluntarioResponsavel;
    }

    public int getIdRestPedido() {
        return idRestPedido;
    }

    public void setIdRestPedido(int idRestPedido) {
        this.idRestPedido = idRestPedido;
    }
    
    public static List<Pedido> getListaPedidosNovos() throws SQLException, ClassNotFoundException {
        List<Pedido> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
        
        String cmd = "SELECT * FROM DIAS_grupo10.Pedido WHERE estadoPedido = 1";
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            
            ResultSet rs = preparedStatement.executeQuery(cmd);
                
            while (rs.next()) {
                    
                Pedido item = new Pedido();
                item.setIdPedido(rs.getInt("idPedido"));
                item.setNumPedido(rs.getString("numPedido"));
                item.setTipoPedido(rs.getString("tipoPedido"));
                item.setTipoPedidoEntrega(rs.getInt("tipoPedidoEntrega"));
                item.setDescricaoPedido(rs.getString("descricaoPedido"));
                item.setEstadoPedido(rs.getInt("estadoPedido"));
                item.setDataPedido(rs.getDate("dataPedido"));
                item.setHoraPedido(rs.getTime("horaPedido"));
                item.setDetalhesPedidos(rs.getString("detalhesPedidos"));
                lst.add(item);
            }
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }

    public static List<Pedido> getListaPedidosByCliente(int id) throws SQLException, ClassNotFoundException {
        List<Pedido> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
        
        String cmd = "SELECT * FROM DIAS_grupo10.Pedido WHERE numPedido like '"+id+"-%'";
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            
            ResultSet rs = preparedStatement.executeQuery(cmd);
                
            while (rs.next()) {
                    
                Pedido item = new Pedido();
                item.setIdPedido(rs.getInt("idPedido"));
                item.setNumPedido(rs.getString("numPedido"));
                item.setTipoPedido(rs.getString("tipoPedido"));
                item.setTipoPedidoEntrega(rs.getInt("tipoPedidoEntrega"));
                item.setDescricaoPedido(rs.getString("descricaoPedido"));
                item.setEstadoPedido(rs.getInt("estadoPedido"));
                item.setDataPedido(rs.getDate("dataPedido"));
                item.setHoraPedido(rs.getTime("horaPedido"));

                item.setDetalhesPedidos(rs.getString("detalhesPedidos"));
                
                lst.add(item);
            }
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }
    
    public static List<Pedido> getListaPedidosProntos(int idVol) throws SQLException, ClassNotFoundException {
        List<Pedido> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
        
        String cmd = "SELECT * FROM DIAS_grupo10.Pedido WHERE estadoPedido = 4 and idVoluntarioResponsavel = "+idVol;
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            
            ResultSet rs = preparedStatement.executeQuery(cmd);
                
            while (rs.next()) {
                    
                Pedido item = new Pedido();
                item.setIdPedido(rs.getInt("idPedido"));
                item.setNumPedido(rs.getString("numPedido"));
                item.setTipoPedido(rs.getString("tipoPedido"));
                item.setTipoPedidoEntrega(rs.getInt("tipoPedidoEntrega"));
                item.setDescricaoPedido(rs.getString("descricaoPedido"));
                item.setEstadoPedido(rs.getInt("estadoPedido"));
                item.setDataPedido(rs.getDate("dataPedido"));
                item.setHoraPedido(rs.getTime("horaPedido"));
                item.setDetalhesPedidos(rs.getString("detalhesPedidos"));
                item.setIdVoluntarioResponsavel(rs.getInt("idVoluntarioResponsavel"));
                lst.add(item);
            }
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }
    
        public static List<Pedido> getListaPedidosEmProcessamento(int idRP) throws SQLException, ClassNotFoundException {
        List<Pedido> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
        
        String cmd = "SELECT * FROM DIAS_grupo10.Pedido WHERE estadoPedido = 2 and idRestPedido = "+idRP;
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            
            ResultSet rs = preparedStatement.executeQuery(cmd);
                
            while (rs.next()) {
                    
                Pedido item = new Pedido();
                item.setIdPedido(rs.getInt("idPedido"));
                item.setNumPedido(rs.getString("numPedido"));
                item.setTipoPedido(rs.getString("tipoPedido"));
                item.setTipoPedidoEntrega(rs.getInt("tipoPedidoEntrega"));
                item.setDescricaoPedido(rs.getString("descricaoPedido"));
                item.setEstadoPedido(rs.getInt("estadoPedido"));
                item.setDataPedido(rs.getDate("dataPedido"));
                item.setHoraPedido(rs.getTime("horaPedido"));
                item.setDetalhesPedidos(rs.getString("detalhesPedidos"));
                lst.add(item);
            }
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }
    
    public static List<Pedido> getListaPedidosEmProcessamentoParaConcluir(int idRP) throws SQLException, ClassNotFoundException {
        List<Pedido> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
        
        String cmd = "SELECT * FROM DIAS_grupo10.Pedido WHERE estadoPedido = 2 and idRestPedido = "+idRP;
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            
            ResultSet rs = preparedStatement.executeQuery(cmd);
                
            while (rs.next()) {
                    
                Pedido item = new Pedido();
                item.setIdPedido(rs.getInt("idPedido"));
                item.setNumPedido(rs.getString("numPedido"));
                item.setTipoPedido(rs.getString("tipoPedido"));
                item.setTipoPedidoEntrega(rs.getInt("tipoPedidoEntrega"));
                item.setDescricaoPedido(rs.getString("descricaoPedido"));
                item.setEstadoPedido(rs.getInt("estadoPedido"));
                item.setDataPedido(rs.getDate("dataPedido"));
                item.setHoraPedido(rs.getTime("horaPedido"));
                item.setDetalhesPedidos(rs.getString("detalhesPedidos"));
                lst.add(item);
            }
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }
}
