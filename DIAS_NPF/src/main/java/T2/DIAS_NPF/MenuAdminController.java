/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import T2.DIAS_NPF.Estatisticas.RestauranteFrequente;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class MenuAdminController implements Initializable {


    @FXML
    private AnchorPane userPNG;
    @FXML
    private Label lblUser;
    @FXML
    private Button btnLogOff;
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    private PieChart pieChart;
    @FXML
    private AnchorPane estatisticasPane;
    @FXML
    private TableView<Estatisticas.RestauranteFrequente> tbVEsatatisticas;
    @FXML
    private TableColumn<Estatisticas.RestauranteFrequente, ?> clnNomeRest;
    @FXML
    private TableColumn<Estatisticas.RestauranteFrequente, ?> clnNumPedidos;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        estatisticasPane.setVisible(false);
        String nome = Helper.getLoginUser().getNomeUser();
        lblUser.setText(nome);
    }    
    
    @FXML
    private void btnLogOff_OnAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Login.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF Application");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.show();
            fechar();
        } catch(IOException e) {
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
        finally{ fechar(); }
         
    }

    @FXML
    private void btn1_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException { //buton estatisticas
        estatisticasPane.setVisible(true);
        ObservableList<Estatisticas.RestauranteFrequente> rf = FXCollections.observableArrayList(Estatisticas.getListaRestMaisFrequentes());
        tbVEsatatisticas.setItems(rf);
        clnNomeRest.setCellValueFactory(new PropertyValueFactory("nome_rest"));
        clnNumPedidos.setCellValueFactory(new PropertyValueFactory("Mais Frequente"));
    }

    @FXML
    private void btn2_OnAction(ActionEvent event) {
    }

    @FXML
    private void btn3_OnAction(ActionEvent event) {
    }

    @FXML
    private void btn4_OnAction(ActionEvent event) {
    }
    
    public void fechar(){
        Window window = userPNG.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
    

    public void Estatisticas123() throws SQLException, ClassNotFoundException {

              
}
}
