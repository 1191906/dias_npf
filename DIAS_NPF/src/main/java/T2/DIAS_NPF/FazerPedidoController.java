/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.net.URL;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class FazerPedidoController implements Initializable {

    @FXML
    private ComboBox<Restaurantes> combBox;
    @FXML
    private TableView<Pratos> tableView;
    @FXML
    private TableColumn<Pratos, String> clnPratos;
    @FXML
    private TableColumn<Pratos, String> clnDescricao;
    @FXML
    private TableColumn<Pratos, Double> clnPreco;
    @FXML
    private Label lbl_nomeCliente;
    @FXML
    private Button btnFinalizar;
    @FXML
    private ListView<Pratos> listView;
    @FXML
    private Button btnAdicionarPrato;
    @FXML
    private Button btnRemoverPrato;
    
    private final ArrayList<Pratos> pratos = new ArrayList();
    private double precoTotal =0; 
    @FXML
    private ComboBox<TipoEntrega> combBoxEntrega;
    @FXML
    private Label nomeCliente;
    @FXML
    private Label numPedido;
    @FXML
    private Label estadoPedido;
    
    private static final Random RANDOM = new SecureRandom();
    @FXML
    private Label precoPedid;
    @FXML
    private Label precoTotalPagar;
    private static int idPedido;
    private static String numPedido1;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            numPedido1 = Helper.getLoginUser().getIdUser()+"-"+generateRandomNumPedido();
            nomeCliente.setText(Helper.getLoginUser().getNomeUser());
            numPedido.setText(numPedido1);
            estadoPedido.setText("Em Espera");
            setNomeUser();
            combBox();
            combBox2();
        } catch (SQLException | ClassNotFoundException ex) {
            String message = "Erro: " + ex.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    }    
    
    //apresentar conteudo combox 
    public void combBox() throws SQLException, ClassNotFoundException{
        ObservableList<Restaurantes> nn = FXCollections.observableArrayList(Restaurantes.getListaRestaurantes());
        combBox.setItems(nn);
        
    }
    //apresentar conteudo combox2 
    public void combBox2() throws ClassNotFoundException, SQLException {
        ObservableList<TipoEntrega> TE = FXCollections.observableArrayList(TipoEntrega.getListaTipoEntrega());
        combBoxEntrega.setItems(TE);
    }
    //ao selecionar conteudo combox 
    public void tableViewByCombBomxSelect() throws SQLException, ClassNotFoundException{
        int id = combBox.getValue().getIdRestaurante();
        
        ObservableList<Pratos> nn = FXCollections.observableArrayList(Pratos.getListaPratos(id));
        tableView.setItems(nn);
        clnPratos.setCellValueFactory(new PropertyValueFactory("tipoPrato"));
        clnDescricao.setCellValueFactory(new PropertyValueFactory("descricaoPrato"));
        clnPreco.setCellValueFactory(new PropertyValueFactory("precoPrato"));
    }

    @FXML
    private void combBox_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException {
        tableViewByCombBomxSelect();
    }
    
    @FXML
    private void combBoxEntrega_OnAction(ActionEvent event) {
        precoTotal = 0;
        precoTotal = getPrecoTotal();
    }
    //botao finalizar
    @FXML
    private void btnFinalizar_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException { 
        
        if (listView.getItems().isEmpty()){
            Helper.showErrorAlert("O seu pedido está vazio!");
        } else if(combBoxEntrega.getSelectionModel().getSelectedItem() == null){
            Helper.showErrorAlert("Selecione um método de Entrega!");
        }else{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("NPF ORDER");
            alert.setHeaderText("O seu pedido está pronto para ser enviado!");
            alert.setContentText("Deseja mesmo continuar?");

            ButtonType buttonTypeYES = new ButtonType("Sim");
            ButtonType buttonTypeNO = new ButtonType("Não");
            alert.getButtonTypes().setAll( buttonTypeYES ,buttonTypeNO); 

            Optional<ButtonType> result = alert.showAndWait();        
            if (result.get() == buttonTypeYES) {
                inserirPedidoBD();
            } else {
                event.consume();
            }
        }
    }
    
    
    //remover prato
    @FXML
    private void btnRemoverPrato_OnAction(ActionEvent event) {
        
        if (listView.getSelectionModel().getSelectedItem() == null) {
            Helper.showErrorAlert("Necessário selecionar uma linha do pedido!");
        return;
        }
        pratos.remove(listView.getSelectionModel().getSelectedItem());
        ObservableList<Pratos> nn = FXCollections.observableArrayList(pratos);
        listView.setItems(nn);

    }
//adicionar prato
    @FXML
    private void btnAdicionarPrato_OnAction(ActionEvent event) {
        
        if (tableView.getSelectionModel().getSelectedItem() == null) {
            Helper.showErrorAlert("Necessário selecionar um prato!");
        return;
        }
        Pratos pratoSelected = tableView.getSelectionModel().getSelectedItem();
        pratos.add(pratoSelected);
        
        ObservableList<Pratos> nn = FXCollections.observableArrayList(pratos);
        
        listView.setItems(nn);
        System.out.println(pratos.toString());
    }
    
    //apresentar nome utilizador
    public void setNomeUser(){
        lbl_nomeCliente.setText(Helper.getLoginUser().getNomeUser()); 
    }
    
    //saber preço total
    public double getPrecoTotal(){           
        double pratoss = 0;
        for (Pratos prato : pratos) {
            precoTotal += prato.getPrecoPrato();
            pratoss = prato.getPrecoPrato();
            System.out.println("Preço do Prato= "+pratoss);
            System.out.println("Preço Total= " +precoTotal);
        }   
        precoPedid.setText(String.valueOf(precoTotal));
        precoTotal += combBoxEntrega.getSelectionModel().getSelectedItem().getPrecoEntrega();
        System.out.println("Preco Final= " +precoTotal);
        precoTotalPagar.setText(String.valueOf(precoTotal));
        
    return precoTotal;
    }
    
    public static String generateRandomNumPedido(){

        String letters = "1234567890123456789123456789012345678912345678901234567891234567890123456789";

        String pw = "";
        for (int i=0; i<6; i++){
            int index = (int)(RANDOM.nextDouble()*letters.length());
            pw += letters.substring(index, index+1);
        }
    return pw;
    }
//inserir pedido
    public void inserirPedidoBD() throws SQLException, ClassNotFoundException{
    
        int tipoEntrega = combBoxEntrega.getSelectionModel().getSelectedItem().getIdEntrega();
        idPedido = getIdPedido();
        String cmd = "Insert Into DIAS_grupo10.Pedido (`idPedido`,`numPedido`,`tipoPedido`,`descricaoPedido`,`tipoPedidoEntrega`,`dataPedido`,`horaPedido`,`estadoPedido`,`idVoluntarioResponsavel`,`detalhesPedidos`,`idRestPedido`) "
                + "VALUES ("+ idPedido +", '"+ numPedido1+ "','Em Espera','Novo Pedido',"+ tipoEntrega +",CURRENT_DATE(),CURRENT_TIME(),1,0,'"+pratos.toString()+"',"+combBox.getValue().getIdRestaurante()+")";
        Helper.InsertUpdateDeleteBD(cmd);
    }
    
    //autoincrementar id
    public int getIdPedido() throws SQLException, ClassNotFoundException{
        
        Connection conn = ConnDB.getConnDB();
        String cmd;
        cmd = "SELECT MAX(idPedido) AS idPedido FROM DIAS_grupo10.Pedido";
        Pedido item = new Pedido();
        
        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        ResultSet rs = preparedStatement.executeQuery(cmd);
         while (rs.next()) {
                    
                    
                    item.setIdPedido(rs.getInt("idPedido"));
                    
                }
                preparedStatement.close();
                
        idPedido = item.getIdPedido();
        System.out.println("id ultimo pedido="+(idPedido));
        idPedido = idPedido + 1;
        System.out.println("id novo pedido="+(idPedido));
    return idPedido;
    }
}
