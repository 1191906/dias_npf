/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class Pratos {
    
    private int idPrato;
    private int idRestaurantePrato;
    private String tipoPrato;
    private String descricaoPrato;
    private double precoPrato;
    
    public static final String SQL_SELECT_PRATOS = "SELECT * FROM DIAS_grupo10.Pratos";

    Pratos(){}
    
    public int getIdPrato() {
        return idPrato;
    }

    public void setIdPrato(int idPrato) {
        this.idPrato = idPrato;
    }

    public int getIdRestaurante() {
        return idRestaurantePrato;
    }

    public void setIdRestaurante(int idRestaurante) {
        this.idRestaurantePrato = idRestaurante;
    }

    public String getTipoPrato() {
        return tipoPrato;
    }

    public void setTipoPrato(String tipoPrato) {
        this.tipoPrato = tipoPrato;
    }

    public String getDescricaoPrato() {
        return descricaoPrato;
    }

    public void setDescricaoPrato(String descricaoPrato) {
        this.descricaoPrato = descricaoPrato;
    }

    public double getPrecoPrato() {
        return precoPrato;
    }

    public void setPrecoPrato(double precoPrato) {
        this.precoPrato = precoPrato;
    }

    public Pratos(int idPrato, int idRestaurante, String tipoPrato, String descricaoPrato, double precoPrato) {
        this.idPrato = idPrato;
        this.idRestaurantePrato = idRestaurante;
        this.tipoPrato = tipoPrato;
        this.descricaoPrato = descricaoPrato;
        this.precoPrato = precoPrato;
    }
    
    public static List<Pratos> getListaPratos(int id) throws SQLException, ClassNotFoundException {
        List<Pratos> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
        
        String cmd = "SELECT * FROM DIAS_grupo10.Pratos WHERE Pratos.idRestaurantePrato = '" + id+ "'" ;
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            
            ResultSet rs = preparedStatement.executeQuery(cmd);
                
            while (rs.next()) {
                    
                Pratos item = new Pratos();
                item.setIdPrato(rs.getInt("idPrato"));
                item.setIdRestaurante(rs.getInt("idRestaurantePrato"));
                item.setTipoPrato(rs.getString("tipoPrato"));
                item.setDescricaoPrato(rs.getString("descricaoPrato"));
                item.setPrecoPrato(rs.getDouble("precoPrato"));
                
                    
                lst.add(item);
            }
            preparedStatement.close();
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }
    public String toString() {
        return this.descricaoPrato;
    }
}
