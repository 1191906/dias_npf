/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class VoluntarioFinalizarPedidosController implements Initializable {


    @FXML
    private Label lbl_nomeCliente;
    @FXML
    private TableView<Pedido> tbVPedidos;
    @FXML
    private TableColumn<Pedido, ?> clnID;
    @FXML
    private TableColumn<Pedido, ?> clnNumPedido;
    @FXML
    private TableColumn<Pedido, ?> clnEstado;
    @FXML
    private TableColumn<Pedido, ?> clnEstado1;
    @FXML
    private Button btnFinalizarPedido;
    @FXML
    private TableColumn<Pedido, ?> clnHora;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lbl_nomeCliente.setText(Helper.getLoginUser().getNomeUser());
        try {
            tableViewShowNUpdate();
            lbl_nomeCliente.setText(Helper.getLoginUser().getNomeUser());
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(VoluntarioFinalizarPedidosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    @FXML
    private void btnFinalizarPedido_OnAction(ActionEvent event) throws ClassNotFoundException, SQLException {
        if (tbVPedidos.getSelectionModel().getSelectedItem() == null) {
            Helper.showErrorAlert("Necessário selecionar um pedido!");
        }else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("NPF ORDER");
            alert.setHeaderText("Está preste a concluir o pedido de um cliente!");
            alert.setContentText("Deseja mesmo continuar?");

            ButtonType buttonTypeYES = new ButtonType("Sim");
            ButtonType buttonTypeNO = new ButtonType("Não");
            alert.getButtonTypes().setAll( buttonTypeYES ,buttonTypeNO); 

            Optional<ButtonType> result = alert.showAndWait();        
            if (result.get() == buttonTypeYES) {                
                FinalizarPedido();
                Helper.showInfoAlert("Pedido concluido com sucesso!");
            } else {
                event.consume();
            }
        tableViewShowNUpdate();
        }
    }
    
    public void tableViewShowNUpdate() throws ClassNotFoundException, SQLException{
        int idVol = Helper.getLoginUser().getIdUser();
        ObservableList<Pedido> pp = FXCollections.observableArrayList(Pedido.getListaPedidosProntos(idVol));
        
        tbVPedidos.setItems(pp);
        clnID.setCellValueFactory(new PropertyValueFactory("idPedido"));
        clnNumPedido.setCellValueFactory(new PropertyValueFactory("numPedido"));
        clnEstado.setCellValueFactory(new PropertyValueFactory("tipoPedido"));
        clnEstado1.setCellValueFactory(new PropertyValueFactory("dataPedido"));
        clnHora.setCellValueFactory(new PropertyValueFactory("horaPedido"));
        
    }
    
    public void FinalizarPedido() throws SQLException, ClassNotFoundException{
        Pedido selectedItem = tbVPedidos.getSelectionModel().getSelectedItem();
        int idPedido = selectedItem.getIdPedido();
        
        String cmd = "UPDATE `DIAS_grupo10`.`Pedido`"
            + "SET `tipoPedido` = 'Concluido' , `descricaoPedido` = 'Novo Pedido' , `dataPedido` = current_date(),`horaPedido` = current_time() , `estadoPedido` = '5'" 
            + "WHERE `idPedido` =" + idPedido;
        
        Helper.InsertUpdateDeleteBD(cmd);
    }    
}
