/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import static T2.DIAS_NPF.TipoUser.SQL_SELECT;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author bruno
 */
public class ChefePedidos {
    private int idChefe;
    private int idRestaurante_chefe;
    private String nomeChefe;
    private static final String SQL_SELECT_CHEFE = "SELECT * FROM DIAS_grupo10.chefePedidos";

    ChefePedidos(){}

    public ChefePedidos(int idChefe, int idRestaurante_chefe, String nomeChefe) {
        this.idChefe = idChefe;
        this.idRestaurante_chefe = idRestaurante_chefe;
        this.nomeChefe = nomeChefe;
    }
 
    public int getIdChefe() {
        return idChefe;
    }

    public void setIdChefe(int idChefe) {
        this.idChefe = idChefe;
    }

    public int getIdRestaurante_chefe() {
        return idRestaurante_chefe;
    }

    public void setIdRestaurante_chefe(int idRestaurante_chefe) {
        this.idRestaurante_chefe = idRestaurante_chefe;
    }

    public String getNomeChefe() {
        return nomeChefe;
    }

    public void setNomeChefe(String nomeChefe) {
        this.nomeChefe = nomeChefe;
    }
    //criar array para apresentar lista
     private static ArrayList<ChefePedidos> getListaChefes() throws ClassNotFoundException, SQLException {
        ArrayList<ChefePedidos> lst1 = new ArrayList();
            
        Connection conn = ConnDB.getConnDB();
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT_CHEFE)) {    
            ResultSet rs = preparedStatement.executeQuery(SQL_SELECT_CHEFE);
            
            while (rs.next()) {
                
                ChefePedidos item = new ChefePedidos();
                item.setIdChefe(rs.getInt("idChefe"));
                item.setIdRestaurante_chefe(rs.getInt("idRestaurante_chefe"));
                item.setNomeChefe(rs.getString("nomeChefe"));
                
                lst1.add(item);
            }
        }
        return lst1;
    } 
    
     
     //retornar tudo em Strings para nao aparecer o caminho do objeto
    @Override
    public String toString() {
        return this.nomeChefe;
    }   
}
