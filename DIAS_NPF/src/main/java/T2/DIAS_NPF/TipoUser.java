/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author bruno
 */
public class TipoUser {
    
    public static final int Admin = 1;
    public static final int ChefePedidos = 2;
    public static final int Voluntario = 3;
    public static final int Cliente = 4;
    private int tipoUser_user;
    private String descricao;
    public static final String SQL_SELECT = ("SELECT idtipoUser, tipoUserDescricao FROM DIAS_GRUPO10.TipoUser");
    
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public int getTipoUser_user() {
        return tipoUser_user;
    }

    public void setTipoUser_user(int tipoUser_user) {
        this.tipoUser_user = tipoUser_user;
    }

    /**
     * Retorna uma lista do tipo Tipos com base na informação que é retornada da base de dados
     * @param filter
     * @return 
     */
    
    public static ArrayList<TipoUser> getLista() throws ClassNotFoundException, SQLException {
        return getLista("");
    }
    public static ArrayList<TipoUser> getLista(String filter) throws ClassNotFoundException, SQLException {   
        return getLista(filter);
    }
    
    
    private static ArrayList<TipoUser> getLst() throws ClassNotFoundException, SQLException {
        ArrayList<TipoUser> lst1 = new ArrayList();
            
        Connection conn = ConnDB.getConnDB();
        String cmd = SQL_SELECT;

            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);

                while (rs.next()) {
                    
                    TipoUser item = new TipoUser();
                    item.setTipoUser_user(rs.getInt("idtipoUser"));
                    item.setDescricao(rs.getString("tipoUserDescricao"));

                    lst1.add(item);
                }
                preparedStatement.close();
        return lst1;
    } 
        @Override
    public String toString() {
        return this.descricao;
    }
}
