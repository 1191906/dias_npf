/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class MenuChefePedidosRestController implements Initializable {


    @FXML
    private AnchorPane userPNG;
    @FXML
    private Label lblUser;
    @FXML
    private Button btnLogOff;
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblUser.setText(Helper.getLoginUser().getNomeUser());
    }    
    
    @FXML
    private void btnLogOff_OnAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Login.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF Application");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.show();
            fechar();
        } catch(IOException e) {     
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
        finally{ fechar(); }
    }

    @FXML
    private void btn1_OnAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("VerPedidosChefe.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF Application");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(IOException e) {     
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void btn2_OnAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FinalizarPedidosChefe.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF Application");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(IOException e) {     
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void btn3_OnAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GerirPratoChefe.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF Application");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(IOException e) {     
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void btn4_OnAction(ActionEvent event) {
    }
       
    public void fechar(){
        Window window = userPNG.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
}
