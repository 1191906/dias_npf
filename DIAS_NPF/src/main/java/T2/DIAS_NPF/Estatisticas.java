/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author bruno
 */
public class Estatisticas {
    private static RestauranteFrequente item = new RestauranteFrequente();
    private static Restaurantes item1 = new Restaurantes();
    
    public static ArrayList<Restaurantes> getListaRestaurantes() throws SQLException, ClassNotFoundException{
        
        ArrayList<Restaurantes> lst = new ArrayList(); 
        Connection conn = ConnDB.getConnDB();
        
        try{
            String cmd = "SELECT * FROM DIAS_grupo10.Restaurantes WHERE NOT idRestaurante = 0;";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);
            
            ResultSet rs = preparedStatement.executeQuery(cmd);
                
            while (rs.next()) {
                
                
                item1.setIdRestaurante(rs.getInt("idRestaurante"));
                item1.setNome_rest(rs.getString("nome_rest"));
                System.out.println(item1.getNome_rest());
                lst.add(item1);
            }
            preparedStatement.close();
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }

        return lst;
    }
    
    public static ArrayList<RestauranteFrequente> getListaRestMaisFrequentes() throws SQLException, ClassNotFoundException{
        
        System.out.println("Entrou no obter lista");
        ArrayList<Restaurantes> list = getListaRestaurantes();

        Restaurantes a = list.get(0);
        Restaurantes b = list.get(1);

        
        ArrayList<RestauranteFrequente> lst = new ArrayList(); 
        Connection conn = ConnDB.getConnDB();

            try {
                String cmd = "SELECT * FROM DIAS_grupo10.RestaurantesMaisFrequentes;";
                PreparedStatement preparedStatement = conn.prepareStatement(cmd);
                
                ResultSet rs = preparedStatement.executeQuery(cmd);
                while(rs.next()){
                    
                   
                    item.setIdRest(rs.getInt("idRestPedido"));
                    item.setCount(rs.getInt("MaisFrequente"));
                    if(item.getIdRest() == a.getIdRestaurante()){item.setNomeRest(a.getNome_rest());}
                    if(item.getIdRest() == b.getIdRestaurante()){item.setNomeRest(b.getNome_rest());}
                    lst.add(item);
                }
                
                 
            } catch(SQLException e){
                String message = "Erro: " + e.getMessage();
                System.out.println(message);
                Helper.showErrorAlert(message);
            }

        return lst;
    }
    
    public static class RestauranteFrequente{
        private int idRest;
        private int count;
        private String nomeRest;

        private RestauranteFrequente(){}

        public int getIdRest() {
            return idRest;
        }

        public void setIdRest(int idRest) {
            this.idRest = idRest;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getNomeRest() {
            return nomeRest;
        }

        public void setNomeRest(String nomeRest) {
            this.nomeRest = nomeRest;
        }

        public RestauranteFrequente(int idRest, int count, String nomeRest) {
            this.idRest = idRest;
            this.count = count;
            this.nomeRest = nomeRest;
        }

    }
}
