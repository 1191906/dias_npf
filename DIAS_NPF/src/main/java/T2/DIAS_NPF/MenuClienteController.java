/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class MenuClienteController implements Initializable {


    @FXML
    private AnchorPane userPNG;
    @FXML
    private Label lblUser;
    @FXML
    private Button btnLogOff;
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    @FXML
    private AnchorPane ClienteTornarVoluntario;
    @FXML
    private TextField txtUser;
    @FXML
    private TextField txtPass;
    @FXML
    private Button btnTornarVoluntario;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ClienteTornarVoluntario.setVisible(false);
        String nome = Helper.getLoginUser().getNomeUser();
        lblUser.setText(nome);
    }     
    
    @FXML
    private void btnLogOff_OnAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Login.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF Application");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:pngs/NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.show();
            fechar();
        } catch(IOException e) {     
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
        finally{ fechar(); }
    }

    @FXML
    private void btn1_OnAction(ActionEvent event) {
         try {
            ClienteTornarVoluntario.setVisible(false);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FazerPedidos.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF ORDER");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch(IOException e) {
            System.out.println(e);
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void btn2_OnAction(ActionEvent event) {
         try {
            ClienteTornarVoluntario.setVisible(false);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("VerPedidos.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("NPF VIEW ORDERS");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch(IOException e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void btn3_OnAction(ActionEvent event) {
        ClienteTornarVoluntario.setVisible(true);
    }

    @FXML
    private void btn4_OnAction(ActionEvent event) {
    }

    public void fechar(){
        Window window = userPNG.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    @FXML
    private void btnTornarVoluntario_click(ActionEvent event) throws SQLException, ClassNotFoundException { 
        Utilizadores user = Helper.getLoginUser();
        
        int idUser = getIdUserMax();
        String nome = user.getNomeUser();
        String morada = user.getMoradaUser();
        String cidade = user.getCidadeUser();
        int telefone = user.getTelefoneUser();
        String userName = txtUser.getText();
        String pass = txtPass.getText();
        
        Helper.InsertUpdateDeleteBD("INSERT INTO `DIAS_grupo10`.`User`\n" +
            "(`idUser`, `userName`, `password`, `nomeUser`, `moradaUser`, `telefoneUser`, `cidadeUser`, `tipoUser_user`)\n" +
            "VALUES\n" +
            "('"+idUser+"', '"+userName+"', '"+pass+"', '"+nome+"', '"+morada+"', '"+telefone+"', '"+cidade+"', '3')");
        
        Helper.showInfoAlert("BemVindo à nossa comunidade voluntário!");
    }
    
    
    public int getIdUserMax() throws SQLException, ClassNotFoundException{
        int id;
        
        Connection conn = ConnDB.getConnDB();
        String cmd = "Select MAX(idUser) AS idUserMax FROM DIAS_grupo10.User";
        Utilizadores item;
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            ResultSet rs = preparedStatement.executeQuery(cmd);
            item = new Utilizadores();
            while(rs.next()){ 
                item.setIdUser(rs.getInt("idUserMax"));
            }    
            
        }
        id = item.getIdUser();
        System.out.println("idUser recebido = "+id);
        id +=1;
        System.out.println("Novo idUser = "+id);
 
    return id;
    }
    
}
