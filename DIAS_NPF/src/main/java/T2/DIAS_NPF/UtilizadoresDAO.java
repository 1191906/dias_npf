/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author bruno
 */
public class UtilizadoresDAO {
    
      
    public static Utilizadores getUtilizadorById(String id) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conn = ConnDB.getConnDB();
        String cmd;
        Utilizadores obj = new Utilizadores();
        try {
            cmd = "SELECT * FROM DIAS_grupo10.User WHERE userName='" + id + "'";
       

            PreparedStatement statement = conn.prepareStatement(cmd);
            
           ResultSet rs = statement.executeQuery(cmd);
 
           System.out.print("*userName= "+id+"*\n");
            while (rs.next()) {
                obj = new Utilizadores(rs.getInt("idUser"), rs.getString("userName"), rs.getString("password") , rs.getInt("tipoUser_user"), rs.getString("nomeUser"));
            }
           statement.close();
       } catch (SQLException ex) {
          String message = ("Erro: " + ex.getMessage());
          System.out.println(message);
          Helper.showErrorAlert(message);
        }
       return obj;
   }
    
    public static ChefePedidos getIdRestByChefePedidos(int id) throws SQLException, ClassNotFoundException{
        Connection conn = ConnDB.getConnDB();
        String cmd;
        ChefePedidos obj = new ChefePedidos();
        try {
            cmd = "SELECT * FROM DIAS_grupo10.chefePedidos WHERE idChefe= " + id;
       
            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);
            while (rs.next()) {
                obj = new ChefePedidos(rs.getInt("idChefe"), rs.getInt("idRestaurante_chefe"), rs.getString("nomeChefe"));
            }
        }catch (SQLException ex) {
            String message = ("Erro: " + ex.getMessage());
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return obj;
    }
}