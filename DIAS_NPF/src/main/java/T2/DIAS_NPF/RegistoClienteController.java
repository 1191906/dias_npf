/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author Óscar Jorge
 */
public class RegistoClienteController implements Initializable {


    @FXML
    private TextField textnUser;
    @FXML
    private TextField textPass;
    @FXML
    private TextField textNome;
    @FXML
    private TextField textMorada;
    @FXML
    private TextField textnif;
    @FXML
    private TextField textnTelef;
    @FXML
    private TextField textCidade;
    private ComboBox<TipoUser> Combobox;
    /**
     * Initializes the controller class.
     */
    
     private void Tipouser() throws ClassNotFoundException, SQLException {
        
        // tipos de moeda
        ObservableList<TipoUser> techno = FXCollections.observableArrayList(TipoUser.getLista());
        Combobox.setItems(techno);          
    }
    private void TipoUtilizador() throws ClassNotFoundException, SQLException{
        // tipo utilizador
        ObservableList<TipoUser> nn = FXCollections.observableArrayList(TipoUser.getLista());
        Combobox.setItems(nn);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void registo(ActionEvent event) throws ClassNotFoundException, IOException {
        
        
     String nomeLogin = textnUser.getText();
        String pass = textPass.getText();
        String nomeUser = textNome.getText();
        String Morada = textMorada.getText();
        int nif = Integer.parseInt(textnif.getText());
        int telefone = Integer.parseInt(textnTelef.getText());
        String cidade = textCidade.getText();
        

        PreparedStatement st = null;
        PreparedStatement lastID = null;
        String cmd;
        
        
        
          
        try {
            Connection conn = ConnDB.getConnDB();

            
            cmd = "SELECT MAX(idUser) AS iduser FROM DIAS_grupo10.User;";
            PreparedStatement preparedStatement = conn.prepareStatement(cmd, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs1 = preparedStatement.getGeneratedKeys();
            rs1 = preparedStatement.executeQuery(cmd);
            rs1.next();   //get ultimo id da bd

            st = conn.prepareStatement("INSERT INTO DIAS_grupo10.User "
                    + "(idUser, userName, password, nomeUser, moradaUser, NifUser, telefoneUser, cidadeUser, tipoUser_user) "
                    + "VALUES "
                    + "(?,?,?,?,?,?,?,?,4)",
                    Statement.RETURN_GENERATED_KEYS);

            st.setInt(1, rs1.getInt("idUser") + 1);
            st.setString(2,nomeUser);
            st.setString(3, pass);
            st.setString(4, nomeLogin);
            st.setString(5, Morada);
            st.setInt(6, nif);
            st.setInt(7, telefone);
            st.setString(8, cidade);
            
            int rowsAffected = st.executeUpdate();

            if (rowsAffected > 0) {
                ResultSet rs = st.getGeneratedKeys();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    System.out.println("Acabou! Id: " + id);
                }
            } else {
                System.out.println("Linhas não afetadas!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            Helper.showInfoAlert("Registo criado com sucesso!");
            
        }
    }

    @FXML
    private void Voltar(ActionEvent event) {
        
                try {
                        System.out.println("Tipo User: *Admin*");
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("Login.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root);
                        Stage stage = new Stage();
                        stage.setTitle("NPF Application");
                        stage.setResizable(false);
                        Image anotherIcon = new Image("file:NPF.jpg");
                        stage.getIcons().add(anotherIcon);
                        stage.setScene(scene);
                        stage.show();
                    }catch(IOException e) {
                        String message = "Erro: " + e.getMessage();
                        System.out.println(message);
                        Helper.showErrorAlert(message);
                    }
    }

    
    
}
