/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class Utilizadores {
        
    private int idUser;
    private String userName;
    private String password;
    private String nomeUser;
    private String moradaUser;
    private int NifUser;
    private int telefoneUser;
    private String cidadeUser;
    private int tipoUser_user;
    
    
    public static final String SQL_SELECT_UTILIZADORES = "SELECT * FROM DIAS_grupo10.User";
    
     public static final String SQL_SELECT_USER = "idUser , userName, password, tipoUser_user, nomeUser FROM DIAS_grupo10.User";

    Utilizadores() {}

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNomeUser() {
        return nomeUser;
    }

    public void setNomeUser(String nomeUser) {
        this.nomeUser = nomeUser;
    }

    public String getMoradaUser() {
        return moradaUser;
    }

    public void setMoradaUser(String moradaUser) {
        this.moradaUser = moradaUser;
    }

    public int getNifUser() {
        return NifUser;
    }

    public void setNifUser(int NifUser) {
        this.NifUser = NifUser;
    }

    public int getTelefoneUser() {
        return telefoneUser;
    }

    public void setTelefoneUser(int telefoneUser) {
        this.telefoneUser = telefoneUser;
    }

    public String getCidadeUser() {
        return cidadeUser;
    }

    public void setCidadeUser(String cidadeUser) {
        this.cidadeUser = cidadeUser;
    }

    public int getTipoUser_user() {
        return tipoUser_user;
    }

    public void setTipoUser_user(int tipoUser_user) {
        this.tipoUser_user = tipoUser_user;
    }
     
    public Utilizadores(int idUser, String userName, String password, int tipoUser_user, String nomeUser){
          
        this.idUser = idUser;
        this.userName = userName;
        this.password = password;
        this.tipoUser_user = tipoUser_user;
        this.nomeUser = nomeUser;
        
    }
    
    public static List<Utilizadores> getListaUtilizadores() throws SQLException, ClassNotFoundException {
        List<Utilizadores> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
                 
        try (PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT_USER)) {
            
            ResultSet rs = preparedStatement.executeQuery(SQL_SELECT_USER);
                
            while (rs.next()) {
                    
                Utilizadores item = new Utilizadores();
                item.setIdUser(rs.getInt("idUser"));
                item.setUserName(rs.getString("userName"));
                item.setPassword(rs.getString("password"));
                item.setTipoUser_user(rs.getInt("tipoUser_user"));
                item.setNomeUser(rs.getString("nomeUser"));
                    
                lst.add(item);
            }
            
        }catch(SQLException e){
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    return lst;
    }
    
}
