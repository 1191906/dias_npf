/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2.DIAS_NPF;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class LoginController implements Initializable {

    @FXML
    private Button btnLogin;
    @FXML
    private TextField txtUser;
    @FXML
    private TextField txtPass;
    @FXML
    private Label registo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         bindForm();
    }    
    
    @FXML
    private void txtPassword_OnKeyReleased(KeyEvent event) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        if (event.getCode() == KeyCode.ENTER) {
            login();
        }
    }

    @FXML
    private void btnLogin_click(ActionEvent event) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        login();
    }

    @FXML
    private void registClick(MouseEvent event) {
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("RegistoCliente.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage stage = (Stage) btnLogin.getScene().getWindow();
            stage.setTitle("NPF Regist Area");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:NPF.jpg");
            stage.getIcons().add(anotherIcon);
            stage.setScene(scene);
            stage.show();
        }catch(IOException e) {
            String message = "Erro: " + e.getMessage();
            System.out.println(message);
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void tornarVoluntarioClick(MouseEvent event) {
         try{
           FXMLLoader loader = new FXMLLoader();
           loader.setLocation(getClass().getResource("RegistoVoluntario.fxml"));
           Parent root = loader.load();
           Scene scene = new Scene(root);
           Stage stage = (Stage) btnLogin.getScene().getWindow();
           stage.setTitle("NPF Regist Area");
           stage.setResizable(false);
           Image anotherIcon = new Image("file:NPF.jpg");
           stage.getIcons().add(anotherIcon);
           stage.setScene(scene);
           stage.show();
       }catch(IOException e) {
           String message = "Erro: " + e.getMessage();
           System.out.println(message);
           Helper.showErrorAlert(message);
       }
        
    }
    
     private void bindForm() {
        txtUser.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtUser.getText().length() > 32) {
                txtUser.deleteNextChar();
                Helper.showInfoAlert("O campo Username não pode exceder 32 caracteres.");
            }

            if (!newValue.matches("^[a-zA-Z0-9]+$")) {
                txtUser.deleteNextChar();
            }
        });

        txtPass.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtPass.getText().length() > 512) {
                txtPass.deleteNextChar();
                Helper.showInfoAlert("O campo Password não pode exceder os 512 caracteres.");
            }
        });
    }
    
    public void login() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
     
        if (validarLogin()) {
                String userName = txtUser.getText();
                Utilizadores user = UtilizadoresDAO.getUtilizadorById(userName);
                
                
            switch (user.getTipoUser_user()) {
                case 1: { //Admins
                    try {
                        System.out.println("Tipo User: *Admin*");
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("MenuAdmin.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root);
                        Stage stage = (Stage) btnLogin.getScene().getWindow();
                        stage.setTitle("NPF Admin Area");
                        stage.setResizable(false);
                        Image anotherIcon = new Image("file:NPF.jpg");
                        stage.getIcons().add(anotherIcon);
                        stage.setScene(scene);
                        stage.show();
                    }catch(IOException e) {
                        String message = "Erro: " + e.getMessage();
                        System.out.println(message);
                        Helper.showErrorAlert(message);
                    }
                        finally{ break;}
                    }
                case 2: {// Chefes de Pedidos

                    try {
                        ChefePedidos chefe = UtilizadoresDAO.getIdRestByChefePedidos(Helper.getLoginUser().getIdUser());
                        Helper.setChefePedidos(chefe);
                        System.out.println("Tipo User: *Chefe de Pedidos*");                        
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("MenuChefePedidosRest.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root);
                        Stage stage = (Stage) btnLogin.getScene().getWindow();
                        stage.setTitle("NPF Chefe Area");
                        stage.setResizable(false);
                        Image anotherIcon = new Image("file:NPF.jpg");
                        stage.getIcons().add(anotherIcon);
                        stage.setScene(scene);
                        stage.show();
                    }catch(IOException e) {
                        String message = "Erro: " + e.getMessage();
                        System.out.println(message);
                        Helper.showErrorAlert(message);
                    }
                        finally{ break;}        
                    }
                case 3: {// Voluntários
                    try {
                        System.out.println("Tipo User: *Voluntario*");
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("MenuVoluntario.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root);
                        Stage stage = (Stage) btnLogin.getScene().getWindow();
                        stage.setTitle("NPF volunteer Area");
                        stage.setResizable(false);
                        Image anotherIcon = new Image("file:NPF.jpg");
                        stage.getIcons().add(anotherIcon);
                        stage.setScene(scene);
                        stage.show();
                    }catch(IOException e) {
                        String message = "Erro: " + e.getMessage();
                        System.out.println(message);
                        Helper.showErrorAlert(message);
                    }
                        finally{ break;}
                    }
                case 4:{ // Clientes
                    try {
                        System.out.println("Tipo User: *Cliente*");
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("MenuCliente.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root);
                        Stage stage = (Stage) btnLogin.getScene().getWindow();
                        stage.setTitle("NPF Client Area");
                        stage.setResizable(false);
                        Image anotherIcon = new Image("file:NPF.jpg");
                        stage.getIcons().add(anotherIcon);
                        stage.setScene(scene);
                        stage.show();
                    }catch(IOException e) {
                            String message = "Erro: " + e.getMessage();
                            System.out.println(message);
                            Helper.showErrorAlert(message);
                    }
                    finally{ break; }
                    }
                default:{
                    Helper.showErrorAlert("O seu user possui um tipo de utilizador inválido, fale com um Administrador para o seu problema ser corrigido!");
                    break;
                }
            }
        }
    }
    
     public boolean validarLogin() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        String userName = txtUser.getText();
        String password = txtPass.getText();
       
        Utilizadores user = UtilizadoresDAO.getUtilizadorById(userName);

        if (user.getIdUser() != 0 && user.getPassword().equals(password)) {
            Helper.setLoginUser(user);
            return true;
        } else {
            Helper.showErrorAlert("Nome de Utilizador ou Password inválido(a).");
            return false;
        }

    }
}
