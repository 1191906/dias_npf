module T2.DIAS_NPF {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;
    requires java.sql;

    opens T2.DIAS_NPF to javafx.fxml;
    exports T2.DIAS_NPF;
}