INSERT INTO `DIAS_grupo10`.`User`
(`idUser`,
`userName`,
`password`,
`nomeUser`,
`moradaUser`,
`NifUser`,
`telefoneUser`,
`cidadeUser`,
`tipoUser_user`)
VALUES
('1', 'admin', 'admin123', 'Administrador', 'Morada', '250577377', '256256256', 'Santa Maria da Feira', '1'),
('2', 'chefePedidos', 'chefe123', 'Paulo Pauleta', 'Restaurante', '0', '030303030', 'Santa Maria da Feira', '2'),
('3', 'jaquim', 'jaf123', 'Joaquim Alberto', 'Mirandela','524658565', '992525236', 'Santa Maria da Feira', 2);

INSERT INTO `DIAS_grupo10`.`Restaurantes`
(`idRestaurante`,
`nome_rest`,
`morada_rest`,
`telefone_rest`,
`nif_rest`,
`website_rest`)
VALUES
('1', 'GreenDeal', 'Rua da Jamaica, Aleixo', '256718239', '999999999', 'GreenDealFood.comes'),
(2, 'Ai Comes Comes','Rua do Bicho, Ultimo a Sair', '256325622','888888888','AicomesComes.Bicho.comes');


INSERT INTO `DIAS_grupo10`.`chefePedidos`
(`idChefe`,
`idRestaurante_chefe`,
`nomeChefe`)
VALUES
('2', '1', 'Paulo Pauleta'),
('3',2,'Joaquim Alberto');

INSERT INTO `DIAS_grupo10`.`TipoUser`
(`idtipoUser`,
`tipoUserDescricao`)
VALUES
(1,'Admin'), (2,'Chefe de Pedidos'), (3,'Voluntario'), (4,'Cliente');

