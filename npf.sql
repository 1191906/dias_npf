
CREATE TABLE DIAS_grupo10.`User` (
    `idUser` int(4)  NOT NULL ,
    `userName` varchar(32)  NOT NULL ,
    `password` varchar(32)  NOT NULL ,
    `nomeUser` Varchar(32)  NOT NULL ,
    `moradaUser` Varchar(50)  NULL ,
    `NifUser` int(9)  NOT NULL ,
    `telefoneUser` Varchar(9)  NOT NULL ,
    `cidadeUser` Varchar(50)  NULL ,
    `tipoUser_user` int(3)  NOT NULL ,
    PRIMARY KEY (
        `idUser`
    ),
    CONSTRAINT `uc_User_userName` UNIQUE (
        `userName`
    ),
    CONSTRAINT `uc_User_NifUser` UNIQUE (
        `NifUser`
    )
);

CREATE TABLE DIAS_grupo10.`TipoUser` (
    `idTipoUser` int(4)  NOT NULL ,
    `tipoUserDescricao` varchar(100)  NOT NULL ,
    PRIMARY KEY (
        `idTipoUser`
    )
);

CREATE TABLE DIAS_grupo10.`Restaurantes` (
    `idRestaurante` int(4)  NOT NULL ,
    `nome_rest` Varchar(32)  NOT NULL ,
    `morada_rest` Varchar(50)  NOT NULL ,
    `telefone_rest` int(9)  NOT NULL ,
    `nif_rest` int(9)  NOT NULL ,
    `website_rest` Varchar(50)  NULL ,
    PRIMARY KEY (
        `idRestaurante`
    ),
    CONSTRAINT `uc_Restaurantes_nome_rest` UNIQUE (
        `nome_rest`
    ),
    CONSTRAINT `uc_Restaurantes_nif_rest` UNIQUE (
        `nif_rest`
    )
);

CREATE TABLE DIAS_grupo10.`Pedido` (
    `idPedido` int(4)  NOT NULL ,
    `numPedido` int(9)  NOT NULL ,
    `tipoPedido` varchar(50)  NOT NULL ,
    `descricaoPedido` varchar(50)  NOT NULL ,
    `tipoPedidoEntrega` int(4)  NOT NULL ,
    `dataPedido` Datetime  NOT NULL ,
    `estadoPedido` int(4)  NOT NULL ,
    PRIMARY KEY (
        `idPedido`
    )
);

CREATE TABLE DIAS_grupo10.`Pratos` (
    `idPrato` int(5)  NOT NULL ,
    `idRestaurantePrato` int(4)  NOT NULL ,
    `tipoPrato` Varchar(50)  NOT NULL ,
    `descricao` Varchar(50)  NOT NULL ,
    `precoPrato` Decimal(18,2)  NOT NULL ,
    PRIMARY KEY (
        `idPrato`
    )
);

CREATE TABLE DIAS_grupo10.`chefePedidos` (
    `idChefe` int(4)  NOT NULL ,
    `idRestaurante_chefe` int(4)  NOT NULL ,
    `nomeChefe` varchar(32)  NOT NULL ,
    PRIMARY KEY (
        `idChefe`
    )
);

CREATE TABLE DIAS_grupo10.`TipoEntrega` (
    `idEntrega` int(4)  NOT NULL ,
    `tipoEntrega` varchar(50)  NOT NULL ,
    `descricaoEntrega` varchar(50)  NOT NULL ,
    `tempoEntrega` Datetime  NOT NULL ,
    `precoEntrega` Decimal(18,2)  NOT NULL ,
    PRIMARY KEY (
        `idEntrega`
    )
);

CREATE TABLE DIAS_grupo10.`TipoEstadoPedido` (
    `idEstadoPedido` int(4)  NOT NULL ,
    `tipoEstadoPedido` varchar(50)  NOT NULL ,
    `descricaoEstadoPedido` varchar(50)  NOT NULL ,
    PRIMARY KEY (
        `idEstadoPedido`
    )
);

CREATE INDEX `idx_User_nomeUser`
ON DIAS_grupo10.`User` (`nomeUser`);

CREATE INDEX `idx_Pedido_descricaoPedido`
ON DIAS_grupo10.`Pedido` (`descricaoPedido`);

CREATE INDEX `idx_Pratos_descricao`
ON DIAS_grupo10.`Pratos` (`descricao`);

ALTER TABLE DIAS_grupo10.`User` ADD CONSTRAINT `fk_User_tipoUser_user` FOREIGN KEY(`tipoUser_user`)
REFERENCES DIAS_grupo10.`TipoUser` (`idTipoUser`);

ALTER TABLE DIAS_grupo10.`Pedido` ADD CONSTRAINT `fk_Pedido_descricaoPedido` FOREIGN KEY(`descricaoPedido`)
REFERENCES DIAS_grupo10.`Pratos` (`descricao`);

ALTER TABLE DIAS_grupo10.`Pedido` ADD CONSTRAINT `fk_Pedido_tipoPedidoEntrega` FOREIGN KEY(`tipoPedidoEntrega`)
REFERENCES DIAS_grupo10.`TipoEntrega` (`idEntrega`);

ALTER TABLE DIAS_grupo10.`Pedido` ADD CONSTRAINT `fk_Pedido_estadoPedido` FOREIGN KEY(`estadoPedido`)
REFERENCES DIAS_grupo10.`TipoEstadoPedido` (`idEstadoPedido`);

ALTER TABLE DIAS_grupo10.`Pratos` ADD CONSTRAINT `fk_Pratos_idRestaurantePrato` FOREIGN KEY(`idRestaurantePrato`)
REFERENCES DIAS_grupo10.`Restaurantes` (`idRestaurante`);

ALTER TABLE DIAS_grupo10.`chefePedidos` ADD CONSTRAINT `fk_chefePedidos_idChefe` FOREIGN KEY(`idChefe`)
REFERENCES DIAS_grupo10.`User` (`idUser`);

ALTER TABLE DIAS_grupo10.`chefePedidos` ADD CONSTRAINT `fk_chefePedidos_nomeChefe` FOREIGN KEY(`nomeChefe`)
REFERENCES DIAS_grupo10.`User` (`nomeUser`);

ALTER TABLE DIAS_grupo10.`chefePedidos` ADD CONSTRAINT `fk_chefePedidos_idRestaurante_chefe` FOREIGN KEY(`idRestaurante_chefe`)
REFERENCES DIAS_grupo10.`Restaurantes` (`idRestaurante`);

